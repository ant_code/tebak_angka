function deklarasi(ronde, angka, s1r1, s2r1) {
    ronde++;
    alert("Nilai yang dicari : " + angka + "\n------------------------------------\nScore\nPlayer 1 : " + s1r1 + "\nPlayer 2 : " + s2r1);
    if (ronde <= 5) {
        conf1 = confirm('Ronde ' + ronde + '?');
    }else if (s1r1 > s2r1) {
        alert("Player 1 Win. Good Jobs");
    }else if (s2r1 > s1r1) {
        alert("Player 2 Win. Good Jobs");
    }else{
        alert("Hasil Seri");
    }
}

alert(`Selamat datang di game tebak angka
kamu diminta untuk menebak angka 1 - 3
dan kamu akan bermain dalam 5 ronde.
Player yang berhasil mengumpulkan tebakan terbanyak akan menang
SELAMAT BERMAIN!!!`);
let ronde = 1;

let conf1 = true;
let s1r1 = 0;
let s2r1 = 0;
while(ronde <= 5 && conf1) {
    let angka = Math.floor(Math.random() * 3) + 1;
    let player1 = prompt('Player 1: masukan angka');
    let player2 = prompt('Player 2: masukan angka');
    
    if (player1 == player2) {
        alert(`Angka tidak boleh sama`);
        conf1 = confirm('Ulangi?');
    }else if (player1 < 1 || player2 < 1) {
        alert(`Angka tidak boleh kurang dari 1`);
        conf1 = confirm('Ulangi?');
    }else if (player1 > 3 || player2 > 3) {
        alert(`Angka tidak boleh lebih dari 3`);
        conf1 = confirm('Ulangi?');
    }else if (player1 != angka && player2 != angka) {
        s1r1 = s1r1 + 0;
        s2r1 = s2r1 + 0;
        alert(`Tidak ada yang benar. HASIL SERI`);
        deklarasi(ronde++, angka, s1r1, s2r1);
    }else if (player1 == angka) {
        s1r1++;
        s2r1 = s2r1 + 0;
        alert(`Player 1 Win`);
        deklarasi(ronde++, angka, s1r1, s2r1);
    }else if (player2 == angka) {
        s1r1 = s1r1 + 0;
        s2r1++;
        alert(`Player 2 Win`);
        deklarasi(ronde++, angka, s1r1, s2r1);
    }
}